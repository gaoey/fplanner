import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { Route, Redirect, Switch } from 'react-router-dom'
import { firebaseAuth } from './config/authFirebase'
import { Main, Register, Login, SuitQuestion } from './scene'
import { auth } from 'firebase';

function PrivateRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => authed === true
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
    />
  )
}

function PublicRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => authed === false
        ? <Component {...props} />
        : <Redirect to='/' />}
    />
  )
}

export default class RouterApp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      authed: false,
      loading: true
    }
  }

  componentDidMount() {
    this.removeListener = firebaseAuth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          authed: true,
          loading: false,
        })
      } else {
        this.setState({
          authed: false,
          loading: false
        })
      }
    })
  }

  componentWillUnmount() {
    this.removeListener()
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <Switch>
          <Route exact path="/" render={(props) => <Main authed={this.state.authed} {...props}/>} />
          <PublicRoute authed={this.state.authed} path="/register" component={Register} />
          <PrivateRoute authed={this.state.authed} path="/suitquestion" component={SuitQuestion} />
          <PublicRoute authed={this.state.authed} path="/login" component={Login} />
        </Switch>
      </Provider>
    )
  }
}

RouterApp.propTypes = {
  store: PropTypes.object.isRequired,
}
