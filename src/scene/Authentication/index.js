import Register from './Register'
import Login from './Login'
import SuitQuestion from './SuitList'

export {
    Register,
    Login,
    SuitQuestion
}