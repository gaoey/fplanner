import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Header, Footer } from '../../../components'
import LoginForm from './LoginForm'
import { login } from '../../../utils/auth'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loginMessage: null
    }
  }

  handleSubmit(value) {
    console.log('yessir', value)
    login(value.email, value.password).catch((error) => {
      console.log("kuy", error)
        this.setState({
          loginMessage: "Invalid Username/Password"
        })
      })
  }

  resetPassword() {
    // resetPassword(this.email.value)
    //   .then(() => this.setState(setErrorMsg(`Password reset email sent to ${this.email.value}.`)))
    //   .catch((error) => this.setState(setErrorMsg(`Email address not found.`)))
  }

  render() {
    return (
      <div>
        <Header />
        <div id="wrapper">
          <div id="main">
            <section id="login">
              <div className="inner">
                <section>
                  <LoginForm onSubmit={this.handleSubmit} />
                </section>
              </div>
            </section>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}

export default Login


