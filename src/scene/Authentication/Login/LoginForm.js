import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Field, reduxForm } from 'redux-form'

let LoginForm = props => {
  const { handleSubmit } = props
  return (
    <form onSubmit={handleSubmit}>
      <div className="field">
        <label htmlFor="email">Email</label>
        <Field name="email" component="input" type="email" />
      </div>
      <div className="field">
        <label htmlFor="password">Password</label>
        <Field name="password" component="input" type="password" />
      </div>
      <ul className="actions">
        <li><button type="submit" className="special" >Submit</button></li>
        <li><Link to={`/register`} className="button fit">Register</Link></li>
      </ul>
    </form>
  )
}

LoginForm = reduxForm({
  form: 'login'
})(LoginForm)

export default LoginForm