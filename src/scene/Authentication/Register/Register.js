import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Header, Footer } from '../../../components'
import { auth } from '../../../utils/auth'
import RegisterForm from './RegisterForm'

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      registerError: null
    }
  }

  handleSubmit(value) {
    if (value.password === value.rePassword) {
      auth(value.email, value.password).catch(e => {
          this.setState({
            registerError: e.message
          })
        })
    } else {
      alert("Password and Re-password are not Equal")
    }
  }

  render() {
    return (
      <div>
        <Header />
        <div id="wrapper">
          <div id="main">
            <section id="register">
              <div className="inner">
                <RegisterForm onSubmit={this.handleSubmit} />
              </div>
            </section>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}

export default Register


