import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'

let RegisterForm = props => {
  const { handleSubmit } = props
  return (
    <form onSubmit={handleSubmit}>
      <div className="field">
        <label htmlFor="email">Email</label>
        <Field name="email" component="input" type="email" />
      </div>
      <div className="field">
        <label htmlFor="password">Password</label>
        <Field name="password" component="input" type="password" />
      </div>
      <div className="field">
        <label htmlFor="re-password">Re-Password</label>
        <Field name="rePassword" component="input" type="password" />
      </div>
      <ul className="actions">
        <li><button type="submit" className="special" >Submit</button></li>
      </ul>
    </form>
  )
}

RegisterForm = reduxForm({
  form: 'register'
})(RegisterForm)

export default RegisterForm