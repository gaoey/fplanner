import {
  SUIT_CHOICE_ACCEPT,
  GET_QUESTION
} from '../../../constants/ActionTypes'
import { combineReducers } from 'redux'

const initialState = {
  data: [],
  loading: false,
  error: ''
}

const suitQuestion = (state = initialState, action) => {
  switch (action.type) {
    case SUIT_CHOICE_ACCEPT.REQUEST:
      return {
        ...state,
        loading: true
      }

    case SUIT_CHOICE_ACCEPT.SUCCESS: {
      return {
        ...state,
        data: state.suitData.push({
          question: action.question,
          answer: action.answer
        })
      }
    }
    default: {
      return state
    }
  }
}

const currentQuestion = (state = initialState, action) => {
  switch (action.type) {
    case GET_QUESTION.REQUEST: {
      return {
        ...state,
        loading: true
      }
    }

    case GET_QUESTION.SUCCESS: {
      return {
        ...state,
        data: action.payload,
        loading: false,
        error: ''
      }
    }
    case GET_QUESTION.FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    }
    default: {
      return state
    }
  }
}

export default combineReducers({
  suitQuestion,
  currentQuestion
})