
import axios from 'axios'
import {
    SUIT_CHOICE_ACCEPT,
    GET_QUESTION
} from '../../../constants/ActionTypes'
import {
    GET_QUESTION_URL
} from '../../../constants/urls'

const addAnswer = (question, answer) => (dispatch) => {
    dispatch({ type: SUIT_CHOICE_ACCEPT.REQUEST })
    dispatch({
        type: SUIT_CHOICE_ACCEPT.SUCCESS,
        question,
        answer
    })
}

const getQuestion = (id) => (dispatch) => {
    dispatch({ type: GET_QUESTION.REQUEST })
    const url = GET_QUESTION_URL.concat(id)
    return axios({
        url,
    }).then((response) => {
        dispatch({
            type: GET_QUESTION.SUCCESS,
            payload: {
                questionName: response.data.question_name,
                answer: response.data.answers,
                questionNo: response.data.question_no
            }
        })
    }).catch((error) => {
        dispatch({
            type: GET_QUESTION.FAILURE,
            payload: error.response.data
        })
    })
}

export {
    addAnswer,
    getQuestion
}