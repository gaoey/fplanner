import Main from './Main'
import {
    Register,
    Login,
    SuitQuestion
} from './Authentication'

export {
    Main,
    Register,
    Login,
    SuitQuestion
}