import {
  SET_AUTH,
} from '../../constants/ActionTypes'
import { combineReducers } from 'redux'

const initialState = {
  data: {},
  loading: false,
  error: ''
}

const isAuthenticated = (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTH:
      return {
        ...state,
        data:  action.payload
      }
    default: {
      return state
    }
  }
}

export default combineReducers({
  isAuthenticated
})