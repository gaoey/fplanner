import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Navbar, Header, Banner, Tile, Contact, Footer } from '../../components'
import { setAuth } from './mainAction'

class Main extends Component {
  constructor(props) {
    super(props)
    this.sectionAboutUs = this.sectionAboutUs.bind(this)
  }

  componentDidMount() {
    this.props.setAuth(this.props.authed)
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.authed !== this.props.authed){
      this.props.setAuth(nextProps.authed)
    }
  }

  sectionAboutUs() {
    return (
      <section id="two">
        <div className="inner">
          <header className="major">
            <h2>About Us</h2>
          </header>
          <p>Nullam et orci eu lorem consequat tincidunt vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus pharetra. Pellentesque condimentum sem. In efficitur ligula tate urna. Maecenas laoreet massa vel lacinia pellentesque lorem ipsum dolor. Nullam et orci eu lorem consequat tincidunt. Vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus amet pharetra et feugiat tempus.</p>
        </div>
      </section>
    )
  }
  render() {
    return (
      <div id="wrapper">
        <Header />
        <Banner />
        <div id="main">
          <section id="one" className="tiles">
            {Tile("Retirement", "Planning for your retirement", "images/pic01.jpg", "#")}
            {Tile("LifeStyle", "Planning for your LifeStyle", "images/pic02.jpg", "#")}
            {Tile("Property", "Planning for your Property", "images/pic03.jpg", "#")}
            {Tile("Education", "Planning for your Education", "images/pic04.jpg", "#")}
            {Tile("Wealth", "Planning for your more Wealth", "images/pic05.jpg", "#")}
            {Tile("Objective", "Planning for your Objective", "images/pic06.jpg", "#")}
          </section>
          {this.sectionAboutUs()}
          <Contact />
        </div>
        <Footer />
      </div>
    )
  }
}

Main.propTypes = {
  auth: PropTypes.bool,
  setAuth: PropTypes.func
}

export default connect(null, {
  setAuth,
})(Main)
