import { SET_AUTH } from '../../constants/ActionTypes'

const setAuth = (auth) => (dispatch, getState) => {
  dispatch({
    type: SET_AUTH,
    payload: auth
  })
}

export {
  setAuth
}