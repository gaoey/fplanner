import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import rootReducer from '../reducer.js'

const middlewares = [
  thunk,
  logger
]

const enhancer = compose(
  applyMiddleware(...middlewares)
)

const configureStore = preloadedState => createStore(
  rootReducer,
  preloadedState,
  enhancer
)

export default configureStore
