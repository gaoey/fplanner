import { ref, firebaseAuth } from '../config/authFirebase'

export function auth(email, pw) {
    console.log("email", email)
    return firebaseAuth().createUserWithEmailAndPassword(email, pw)
        .then(saveUser)
}

export function logout() {
    firebaseAuth().signOut()
}

export function login(email, pw) {
    return firebaseAuth().signInWithEmailAndPassword(email, pw)
}

export function resetPassword(email) {
    return firebaseAuth().sendPasswordResetEmail(email)
}

export function getTokn() {
    return firebaseAuth().instance.currentUser.getToken()
}

export function saveUser(user) {
    console.log('hey', user)
    return ref.child(`users/${user.uid}/info`)
        .set({
            email: user.email,
            uid: user.uid
        })
        .then(() => user)
}
