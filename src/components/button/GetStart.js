import React, { Component } from 'react'

const GetStart = (style) => {
    return (
        <a href="#main" className={style}>Get Started</a>
    )
}

export default GetStart