import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Question extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedOption: 'option1'
    }
    this.handleOptionChange = this.handleOptionChange.bind(this)
  }

  handleOptionChange(changeEvent) {
    this.setState({
      selectedOption: changeEvent.target.value
    })
  }

  render() {
    const { selectedOption } = this.state
    const {
      question,
      choiceOne,
      choiceTwo,
      choiceThree,
      choiceFour,
      action
    } = this.props
    return (
      <section id="risk-management">
        <div class="container">
          <div>
            <h3 id="question">{question}</h3>
            <div class="form-check">
              <label>
                <input
                  type="radio"
                  name="radio"
                  vale="option1"
                  check={selectedOption === 'option1'}
                  onChange={this.handleOptionChanges} />
                <span class="label-text one-choice" id="radio-one-choice">{choiceOne}</span>
              </label>
            </div>
            <div class="form-check">
              <label>
                <input
                  type="radio"
                  name="radio"
                  value='option2'
                  check={selectedOption === 'option2'}
                  onChange={this.handleOptionChanges} />
                <span class="label-text two-choice">{choiceTwo}</span>
              </label>
            </div>
            <div class="form-check">
              <label>
                <input
                  type="radio"
                  name="radio"
                  value='option3'
                  check={selectedOption === 'option3'}
                  onChange={this.handleOptionChanges} />
                <span class="label-text three-choice">{choiceThree}</span>
              </label>
            </div>
            <div class="form-check">
              <label>
                <input
                  type="radio"
                  name="radio"
                  value='option4'
                  check={selectedOption === 'option4'}
                  onChange={this.handleOptionChanges} />
                <span class="label-text four-choice">{choiceFour}</span>
              </label>
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-primary btn-xl" id="next" onClick={action(question, selectedOption)}>NEXT</button>
          </div>
        </div>
      </section>
    )
  }
}

Question.propTypes = {
  question: PropTypes.string,
  choiceOne: PropTypes.string,
  choiceTwo: PropTypes.string,
  choiceThree: PropTypes.string,
  choiceFour: PropTypes.string,
  action: PropTypes.func
}

Question.defaultProps = {
  question: '',
  choiceOne: '',
  choiceTwo: '',
  choiceThree: '',
  choiceFour: '',
  action: () => { }
}


export default Question