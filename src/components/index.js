import Navbar from './Navbar'
import Header from './Header'
import Banner from './Banner'
import Tile from './Tile'
import Contact from './Contact'
import Footer from './Footer'
import { GetStart } from './button'
import Question from './Question'

export {
    Navbar,
    Header,
    Banner,
    GetStart,
    Tile,
    Contact,
    Footer,
    Question
}