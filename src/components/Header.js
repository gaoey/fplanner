import React, { Component } from 'react'
import { Navbar } from './index'

class Header extends Component {
    render() {
        return (
            <div>
                <header id="header" className="alt">
                    <a href="/" className="logo"><strong>FPLANNER</strong></a>
                    <nav>
                        <a href="#menu">Menu</a>
                    </nav>
                </header>
                <Navbar />
            </div>
        )
    }
}

export default Header