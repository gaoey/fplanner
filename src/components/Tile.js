import React, { Component } from 'react'
import { GetStart } from './index'

const Tile = (topic, description, image, link, alt = '') => {
    return (
        <article>
            <span className="image">
                <img src="{image}" alt="{alt}" />
            </span>
            <header className="major">
                <h3><a href="{link}" className="link">{topic}</a></h3>
                <p>{description}</p>
            </header>
        </article>
    )
}


export default Tile