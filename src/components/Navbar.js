import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { GetStart } from './index'
import { logout } from '../utils/auth'

class Navbar extends Component {
	render() {
		return (
			<nav id="menu">
				<ul className="links">
					<li><a href="/">Home</a></li>
					<li><a href="#two">About Us</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
				<ul className="actions vertical">
					<li>{GetStart("button special fit")}</li>
					<li>
						{
							this.props.isAuthenticated
								? <Link to="/"><button onClick={logout()} className="button fit">Logout</button></Link>
								: <span>
									<Link to="/login" className="button fit">Login</Link>
									<Link to="/register" className="button fit">Register</Link>
								</span>
						}
					</li>
				</ul>
			</nav>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		isAuthenticated: state.auth.isAuthenticated.data
	}
}

export default connect(mapStateToProps, null)(Navbar)