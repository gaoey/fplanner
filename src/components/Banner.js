import React, { Component } from 'react'
import { GetStart } from '../components'

class Banner extends Component {
    render() {
        return (
            <section id="banner" className="major">
                <div className="inner">
                    <header className="major">
                        <h1>FPLANNER</h1>
                    </header>
                    <div className="content">
                        <p>Application for planning your lifestyle to acheive your goal</p>
                        <ul className="actions">
                            <li>{GetStart("button next scrolly")}</li>
                        </ul>
                    </div>
                </div>
            </section>
        )
    }
}

export default Banner