
import firebase from 'firebase'

const firebase_config = {
  apiKey: 'AIzaSyAHAlua4cfKKCiTCqxLNlwVaYP3EGGfakw',
  authDomain: 'f-planner.firebaseapp.com',
  databaseURL: 'https://f-planner.firebaseio.com',
  projectId: 'f-planner',
  storageBucket: '',
  messagingSenderId: '596434305611'
}

firebase.initializeApp(firebase_config)

export const ref = firebase.database().ref()
export const firebaseAuth = firebase.auth