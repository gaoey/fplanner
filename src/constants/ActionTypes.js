import { buildActionTypes } from '../utils/buildActions'

export const SUIT_CHOICE_ACCEPT = buildActionTypes('SUIT_CHOICE')
export const GET_QUESTION = buildActionTypes('GET_QUESTION')
export const SET_AUTH = 'SET_AUTH'