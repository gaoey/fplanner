import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import suitReducer from './scene/Authentication/SuitList/suitReducer'
import auth from './scene/Main/mainReducer'

const rootReducer = combineReducers({
    suitReducer,
    auth,
    form: formReducer
})

export default rootReducer
