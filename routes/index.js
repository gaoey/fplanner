var express = require('express')
var router = express.Router();

/* GET home page. */
router.get('*', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET Register page. */
// router.get('/register', function (request, response){
//   response.render('register', { title: 'Register' });
// })

module.exports = router;
